import { ApiProperty } from '@nestjs/swagger';

export class CreateMessageDto {
  @ApiProperty({
    description: 'message',
    type: String,
    example: 'example message!',
    required: true,
  })
  message: string;
}
