import { SNS, PublishCommand, PublishCommandInput } from '@aws-sdk/client-sns';
import { SQS, ReceiveMessageCommandInput, SendMessageCommandInput, SendMessageCommand } from '@aws-sdk/client-sqs';
import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  private readonly _sns: SNS;
  private readonly _sqs: SQS;

  constructor() {
    this._sns = new SNS({
      region: process.env.AWS_REGION,
      credentials: {
        accessKeyId: process.env.AWS_ACCESS_KEY,
        secretAccessKey: process.env.AWS_SECRET_KEY
      }
    });
    this._sqs = new SQS({
      region: process.env.AWS_REGION,
      credentials: {
        accessKeyId: process.env.AWS_ACCESS_KEY,
        secretAccessKey: process.env.AWS_SECRET_KEY
      }
    })
  }

getSNS(): SNS {
  return this._sns;
}

  getSQS(): SQS {
    return this._sqs;
  }

  async publishMessage(message: string) {
    let params: PublishCommandInput = {
      Message: message,
      MessageGroupId: process.env.AWS_SNS_GROUP_NAME,
      TopicArn: process.env.AWS_SNS_TOPIC_ARN,
    }
    try {
      const data = await this.getSNS().send(new PublishCommand(params))
      console.log("Success.", data);
    } catch (err) {
      console.log("Error", err.stack);
    }
  }

  async getQueueUrl(queueName: string): Promise<string> {
    try {
      const response = await this.getSQS().getQueueUrl({
        QueueName: queueName
      });
      return response.QueueUrl;
    } catch (err) {
      console.log("Error", err.stack);
    }
  }

  async getMessages(queueName: string) {
    try {
      const queueUrl: string = await this.getQueueUrl(queueName);
      let params: ReceiveMessageCommandInput = {
        QueueUrl: queueUrl,
        MaxNumberOfMessages: 10
      }
      const messages = await this.getSQS().receiveMessage(params);
      return messages;
    } catch (err) {
      console.log("Error", err.stack);
    }
  }
}
