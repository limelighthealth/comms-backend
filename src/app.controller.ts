import { Body, Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { CreateMessageDto } from './dto/create-message.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('/message')
  async postMessage(@Body() createMessageDto: CreateMessageDto) {
    const snsResponse = await this.appService.publishMessage(createMessageDto.message);
  }

  @Get('/messages')
  async getQueueUrl(): Promise<any> {
    const queueName = process.env.AWS_SQS_NAME;
    const response = await this.appService.getMessages(queueName);
    return response;
  }
}
