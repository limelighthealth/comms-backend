## About

This is a POC repo that uses AWS SNS and SQS to create messages and store them in a queue. 

## AWS Setup

To run this project, you must have an SNS topic and an SQS Queue created, and the queue must be subscribed to the topic. Additionally, you must configure an IAM user that has access to SNS and SQS. For more information on exactly how to setup SNS and SQS in the AWS console, contact Michael Stirchak.

## Credential Setup

Once you have the AWS services created, create a `.env` file in the root of this project with the following fields:

```
AWS_REGION=us-west-1
AWS_ACCESS_KEY=<your-users-access-key>
AWS_SECRET_KEY=<your-users-secret-key>
AWS_SNS_TOPIC_ARN=<your-sns-arn-url>
AWS_SNS_GROUP_NAME=testGroupId //this can be any string
AWS_SQS_NAME=<name-of-your-sqs-queue>
```

## Running the project

```
yarn start
```

This project has a swagger page that can now be accessed at `localhost:8080/api-docs`